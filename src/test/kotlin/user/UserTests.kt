package user

import org.junit.jupiter.api.Test
import billing.CreditPayment
import billing.DebitPayment
import shipping.ShippingAddress
import java.util.*
import kotlin.test.assertEquals

class UserTests {
    @Test
    fun addCreditPaymentOptionTest() {
        val creditPaymentId = UUID.randomUUID()
        val creditPaymentAlias = "TestCreditPayment"

        val user = User(UUID.randomUUID())
        val paymentOption = CreditPayment(creditPaymentId, creditPaymentAlias)

        user.addPaymentOption(paymentOption)

        assertEquals(1, user.getPaymentOptions().size)
        assertEquals(creditPaymentId, user.getPaymentOptions().first().id)
        assertEquals(creditPaymentAlias, user.getPaymentOptions().first().alias)
    }

    @Test
    fun addDebitPaymentOptionTest() {
        val debitPaymentId = UUID.randomUUID()
        val debitPaymentAlias = "TestDebitPayment"

        val user = User(UUID.randomUUID())
        val paymentOption = DebitPayment(debitPaymentId, debitPaymentAlias)

        user.addPaymentOption(paymentOption)

        assertEquals(1, user.getPaymentOptions().size)
        assertEquals(debitPaymentId, user.getPaymentOptions().first().id)
        assertEquals(debitPaymentAlias, user.getPaymentOptions().first().alias)
    }

    @Test
    fun addCreditAndDebitPaymentOptionsTest() {
        val debitPaymentId = UUID.randomUUID()
        val debitPaymentAlias = "TestDebitPayment"

        val creditPaymentId = UUID.randomUUID()
        val creditPaymentAlias = "TestCreditPayment"

        val user = User(UUID.randomUUID())
        val debitPaymentOption = DebitPayment(debitPaymentId, debitPaymentAlias)
        val creditPaymentOption = CreditPayment(creditPaymentId, creditPaymentAlias)

        user.addPaymentOption(debitPaymentOption)
        user.addPaymentOption(creditPaymentOption)

        assertEquals(2, user.getPaymentOptions().size)
        assertEquals(debitPaymentId, user.getPaymentOptions()[0].id)
        assertEquals(creditPaymentId, user.getPaymentOptions()[1].id)
        assertEquals(debitPaymentAlias, user.getPaymentOptions()[0].alias)
        assertEquals(creditPaymentAlias, user.getPaymentOptions()[1].alias)
    }

    @Test
    fun removePaymentOptionTest() {
        val creditPaymentId = UUID.randomUUID()
        val creditPaymentAlias = "TestCreditPayment"

        val user = User(UUID.randomUUID())
        val paymentOption = CreditPayment(creditPaymentId, creditPaymentAlias)

        user.addPaymentOption(paymentOption)

        assertEquals(1, user.getPaymentOptions().size)

        user.removePaymentOptionById(creditPaymentId)

        assertEquals(0, user.getPaymentOptions().size)
    }

    @Test
    fun removePaymentOptionNoOptionsTest() {
        val creditPaymentId = UUID.randomUUID()
        val user = User(UUID.randomUUID())

        user.removePaymentOptionById(creditPaymentId)

        assertEquals(0, user.getPaymentOptions().size)
    }

    @Test
    fun addShippingAddressOptionTest() {
        val shippingAddressId = UUID.randomUUID()
        val shippingAddressAlias = "Test Address"

        val user = User(UUID.randomUUID())
        val shippingAddressOption = ShippingAddress(shippingAddressId, shippingAddressAlias)

        user.addShippingAddressOption(shippingAddressOption)

        assertEquals(1, user.getShippingAddressOptions().size)
        assertEquals(shippingAddressId, user.getShippingAddressOptions().first().id)
        assertEquals(shippingAddressAlias, user.getShippingAddressOptions().first().alias)
    }

    @Test
    fun removeShippingAddressOptionTest() {
        val shippingAddressId = UUID.randomUUID()
        val shippingAddressAlias = "Test Address"

        val user = User(UUID.randomUUID())
        val shippingAddressOption = ShippingAddress(shippingAddressId, shippingAddressAlias)

        user.addShippingAddressOption(shippingAddressOption)

        assertEquals(1, user.getShippingAddressOptions().size)

        user.removeShippingAddressOptionById(shippingAddressId)

        assertEquals(0, user.getShippingAddressOptions().size)
    }
}