package order

import order.enums.CouponType
import product.Product
import java.util.*
import kotlin.test.Test
import kotlin.test.assertEquals

class OrderTests {
    // Add single product to order.
    // Expectation: The product is added successfully.
    @Test
    fun addProductToOrderTest() {
        val productId = UUID.randomUUID()

        val order = Order()
        val product = Product(productId, "Baseball Bat", 30.00F)

        order.addProduct(product)

        assertEquals(1, order.getProducts().size)
        assertEquals(productId, order.getProducts().first().id)
        assertEquals("Baseball Bat", order.getProducts().first().name)
    }

    // Add many product validation.
    // Expectation: The products are added successfully.
    @Test
    fun addManyProductsToOrderTest(){
        val firstProductId = UUID.randomUUID()
        val secondProductId = UUID.randomUUID()

        val order = Order()
        val firstProduct = Product(firstProductId, "Baseball Bat", 30.00F)
        val secondProduct = Product(secondProductId, "Tennis Racket", 25.00F)

        val productList = mutableListOf(firstProduct, secondProduct)

        order.addManyProducts(productList)

        assertEquals(2, order.getProducts().size)
        assertEquals(firstProductId, order.getProducts()[0].id)
        assertEquals(secondProductId, order.getProducts()[1].id)
        assertEquals("Baseball Bat", order.getProducts()[0].name)
        assertEquals("Tennis Racket", order.getProducts()[1].name)
    }

    @Test
    fun calculateTotalSingleProductTest() {
        val productId = UUID.randomUUID()

        val order = Order()
        val product = Product(productId, "Baseball Bat", 30.00F)

        order.addProduct(product)
        order.calculateTotal()

        assertEquals(30.00F, order.getTotal())
    }

    @Test
    fun calculateTotalManyProductsTest(){
        val firstProductId = UUID.randomUUID()
        val secondProductId = UUID.randomUUID()

        val order = Order()
        val firstProduct = Product(firstProductId, "Baseball Bat", 30.00F)
        val secondProduct = Product(secondProductId, "Tennis Racket", 25.00F)

        val productList = mutableListOf(firstProduct, secondProduct)

        order.addManyProducts(productList)
        order.calculateTotal()

        assertEquals(55F, order.getTotal())
    }

    @Test
    fun calculateTotalAfterProductRemovalTest(){
        val firstProductId = UUID.randomUUID()
        val secondProductId = UUID.randomUUID()

        val order = Order()
        val firstProduct = Product(firstProductId, "Baseball Bat", 30.00F)
        val secondProduct = Product(secondProductId, "Tennis Racket", 25.00F)

        val productList = mutableListOf(firstProduct, secondProduct)

        order.addManyProducts(productList)
        order.calculateTotal()

        assertEquals(55F, order.getTotal())

        order.removeProductById(firstProductId)
        order.calculateTotal()

        assertEquals(25F, order.getTotal())
    }

    @Test
    fun calculateTotalNoProductsTest() {
        val order = Order()

        order.calculateTotal()

        assertEquals(0F, order.getTotal())
    }

    // Add many product with one case of duplicate item id.
    // Expectation: The products are added successfully.
    // Reasoning: Product ids are only unique to the product, not the product instance.
    @Test
    fun addManyProductsToOrderWithDuplicatesTest(){
        val firstProductId = UUID.randomUUID()
        val secondProductId = UUID.randomUUID()
        val thirdProductId = firstProductId

        val order = Order()
        val firstProduct = Product(firstProductId, "Baseball Bat", 30.00F)
        val secondProduct = Product(secondProductId, "Tennis Racket", 25.00F)
        val thirdProduct = Product(thirdProductId, "Baseball Bat", 30.00F)

        val productList = mutableListOf(firstProduct, secondProduct, thirdProduct)

        order.addManyProducts(productList)

        assertEquals(3, order.getProducts().size)
        assertEquals(firstProductId, order.getProducts()[0].id)
        assertEquals(secondProductId, order.getProducts()[1].id)
        assertEquals(thirdProductId, order.getProducts()[2].id)
        assertEquals("Baseball Bat", order.getProducts()[0].name)
        assertEquals("Tennis Racket", order.getProducts()[1].name)
        assertEquals("Baseball Bat", order.getProducts()[2].name)
    }

    // Add single product with one case of duplicate product id.
    // Expectation: The products are added successfully
    // Reasoning: Product ids are only unique to the product, not the product instance. Selection type one vs many shouldn't matter.
    @Test
    fun addDuplicateProductToOrderTest(){
        val productId = UUID.randomUUID()

        val order = Order()
        val product = Product(productId, "Baseball Bat", 30.00F)

        order.addProduct(product)
        order.addProduct(product)

        assertEquals(2, order.getProducts().size)
        assertEquals(productId, order.getProducts()[0].id)
        assertEquals(productId, order.getProducts()[1].id)
        assertEquals("Baseball Bat", order.getProducts()[0].name)
        assertEquals("Baseball Bat", order.getProducts()[1].name)
    }

    // Remove single product from order.
    // Expectation: The product is removed successfully.
    @Test
    fun removeProductFromOrderTest(){
        val productId = UUID.randomUUID()

        val order = Order()
        val product = Product(productId, "Baseball Bat", 30.00F)

        order.addProduct(product)
        order.removeProductById(productId)

        assertEquals(0, order.getProducts().size)
    }

    // Remove single product from empty order.
    // Expectation: No error occurs and order doesn't change.
    @Test
    fun removeNonExistentProductFromOrderTest(){
        val productId = UUID.randomUUID()
        val order = Order()

        order.removeProductById(productId)

        assertEquals(0, order.getProducts().size)
    }

    // Remove duplicate product.
    // Expectation: One of the duplicate products is removed, the other is unaffected.
    @Test
    fun removeDuplicateProductFromOrderTest(){
        val productId = UUID.randomUUID()

        val order = Order()
        val product = Product(productId, "Baseball Bat", 30.00F)

        order.addProduct(product)
        order.addProduct(product)
        order.removeProductById(productId)

        assertEquals(1, order.getProducts().size)
        assertEquals(productId, order.getProducts().first().id)
        assertEquals("Baseball Bat", order.getProducts().first().name)
    }

    // Remove all products from order.
    // Expectation: All products are removed successfully.
    @Test
    fun removeAllProductsFromOrderTest(){
        val firstProductId = UUID.randomUUID()
        val secondProductId = UUID.randomUUID()

        val order = Order()
        val firstProduct = Product(firstProductId, "Baseball Bat", 30.00F)
        val secondProduct = Product(secondProductId, "Tennis Racket", 25.00F)

        val productList = mutableListOf(firstProduct, secondProduct)

        order.addManyProducts(productList)
        order.removeAllProducts()

        assertEquals(0, order.getProducts().size)
    }

    // Remove all products from order with no products.
    // Expectation: No error occurs and order doesn't change.
    @Test
    fun removeAllProductsFromOrderWithNoProductsTest(){
        val order = Order()

        order.removeAllProducts()

        assertEquals(0, order.getProducts().size)
    }

    // Add single coupon to order.
    // Expectation: Coupons list is updated.
    @Test
    fun addSingleCouponToOrderTest(){
        val couponId = UUID.randomUUID()
        val couponType = CouponType.FLAT_TOTAL

        val order = Order()
        val coupon = Coupon(couponId, couponType)

        order.addCoupon(coupon)

        assertEquals(1, order.getCoupons().size)
        assertEquals(couponId, order.getCoupons().first().id)
        assertEquals(couponType, order.getCoupons().first().type)
    }

    @Test
    fun applySingleFlatCouponTest(){
        val couponId = UUID.randomUUID()
        val couponType = CouponType.FLAT_TOTAL

        val product = Product(UUID.randomUUID(), "Baseball Bat", 15.00F)
        val order = Order()
        order.addProduct(product)

        val coupon = Coupon(couponId, couponType)
        coupon.setFlatValue(10.01F)

        order.addCoupon(coupon)
        order.applyCoupons()
        order.calculateTotal()

        assertEquals(4.99F, order.getTotal())
    }

    @Test
    fun organizeCouponsTest() {
        val percentageProductCouponId = UUID.randomUUID()
        val percentageTotalCouponId = UUID.randomUUID()
        val freeProductCouponId = UUID.randomUUID()
        val flatTotalCouponId = UUID.randomUUID()
        val flatProductCouponId = UUID.randomUUID()
        val bogoFreeCouponId = UUID.randomUUID()
        val bogoFlatCouponId = UUID.randomUUID()
        val bogoPercentageCouponId = UUID.randomUUID()

        val percentageProductCoupon = Coupon(percentageProductCouponId, CouponType.PERCENTAGE_PRODUCT)
        val percentageTotalCoupon = Coupon(percentageTotalCouponId, CouponType.PERCENTAGE_TOTAL)
        val freeProductCoupon = Coupon(freeProductCouponId, CouponType.FREE_PRODUCT)
        val flatTotalCoupon = Coupon(flatTotalCouponId, CouponType.FLAT_TOTAL)
        val flatProductCoupon = Coupon(flatProductCouponId, CouponType.FLAT_PRODUCT)
        val bogoFreeCoupon = Coupon(bogoFreeCouponId, CouponType.BOGO_FREE)
        val bogoFlatCoupon = Coupon(bogoFlatCouponId, CouponType.BOGO_FLAT)
        val bogoPercentageCoupon = Coupon(bogoPercentageCouponId, CouponType.BOGO_PERCENTAGE)

        val order = Order()

        order.addCoupon(percentageProductCoupon)
        order.addCoupon(percentageTotalCoupon)
        order.addCoupon(freeProductCoupon)
        order.addCoupon(flatTotalCoupon)
        order.addCoupon(flatProductCoupon)
        order.addCoupon(bogoFreeCoupon)
        order.addCoupon(bogoFlatCoupon)
        order.addCoupon(bogoPercentageCoupon)

        order.organizeCoupons()

        assertEquals(CouponType.FREE_PRODUCT, order.getCoupons()[0].type)
        assertEquals(CouponType.BOGO_FREE, order.getCoupons()[1].type)
        assertEquals(CouponType.FLAT_PRODUCT, order.getCoupons()[2].type)
        assertEquals(CouponType.BOGO_FLAT, order.getCoupons()[3].type)
        assertEquals(CouponType.PERCENTAGE_PRODUCT, order.getCoupons()[4].type)
        assertEquals(CouponType.BOGO_PERCENTAGE, order.getCoupons()[5].type)
        assertEquals(CouponType.FLAT_TOTAL, order.getCoupons()[6].type)
        assertEquals(CouponType.PERCENTAGE_TOTAL, order.getCoupons()[7].type)
    }
}