package user

import order.Order
import billing.IPayment
import shipping.ShippingAddress
import java.util.*

class User(val id: UUID) {
    private var order: Order? = null
    private var preferredShippingAddress: ShippingAddress? = null
    private var shippingAddressOptions: MutableList<ShippingAddress> = mutableListOf()
    private var paymentOptions: MutableList<IPayment> = mutableListOf()

    // Order
    fun getOrder(): Order? {
        return order
    }

    // Preferred shipping address
    fun setPreferredShippingAddress(shippingAddress: ShippingAddress) {
        preferredShippingAddress = shippingAddress
    }

    fun getPreferredShippingAddress(): ShippingAddress? {
        return preferredShippingAddress
    }

    // Shipping address
    fun getShippingAddressOptions(): MutableList<ShippingAddress> {
        return shippingAddressOptions
    }

    fun addShippingAddressOption(shippingAddress: ShippingAddress) {
        shippingAddressOptions.add(shippingAddress)
    }

    fun removeShippingAddressOptionById(addressOptionId: UUID) {
        for(addressOption in shippingAddressOptions) {
            if(addressOption.id == addressOptionId) {
                shippingAddressOptions.remove(addressOption)
                break
            }
        }
    }

    // Payment options
    fun getPaymentOptions(): MutableList<IPayment> {
        return paymentOptions
    }

    fun addPaymentOption(paymentOption: IPayment) {
        paymentOptions.add(paymentOption)
    }

    fun removePaymentOptionById(paymentOptionId: UUID) {
        for(paymentOption in paymentOptions) {
            if(paymentOption.id == paymentOptionId) {
                paymentOptions.remove(paymentOption)
                break
            }
        }
    }
}