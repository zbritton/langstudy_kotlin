package billing

import java.util.*

interface IPayment {
    var id: UUID
    var alias: String
}