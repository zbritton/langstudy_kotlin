package billing

import billing.enums.CardProvider
import java.util.*

class CreditPayment(override var id: UUID, override var alias: String) : IPayment {
    private var cardNumber: Int? = null
    private var cardExpiration: Date? = null
    private var cardOwnerNameFirst: String? = null
    private var cardOwnerNameSecond: String? = null
    private var cardBillingAddress: BillingAddress? = null
    private var cardProvider: CardProvider? = null
}