package billing

import billing.enums.BankAccountType
import java.util.*

class DebitPayment(override var id: UUID, override var alias: String) : IPayment {
    private var bankAccountType: BankAccountType? = null
    private var bankAccountNumber: Int? = null
    private var bankRoutingNumber: Int? = null
}