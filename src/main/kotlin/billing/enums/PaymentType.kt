package billing.enums

enum class PaymentType {
    CREDIT_CARD,
    BANK_ACCOUNT
}
