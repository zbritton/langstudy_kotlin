package billing.enums

enum class CardProvider {
    VISA,
    MASTERCARD,
    AMEX
}