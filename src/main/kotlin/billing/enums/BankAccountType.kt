package billing.enums

enum class BankAccountType {
    CHECKING,
    SAVING
}
