package shipping

import java.util.*

class ShippingAddress(val id: UUID, val alias: String) {
    private val lineOne: String? = null
    private val lineTwo: String? = null
    private val additionalLine: String? = null
    private val country: String? = null
    private val city: String? = null
    private val state: String? = null
    private val zipCode: String? = null
}