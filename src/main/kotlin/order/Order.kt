package order

import order.enums.CouponType
import product.Product
import java.util.*

class Order {
    private var total: Float = 0.0F
    private var products: MutableList<Product> = mutableListOf()
    private var coupons: MutableList<Coupon> = mutableListOf()

    fun addProduct(selectedProduct: Product, count: Int = 1) {
        for(i in 1..count) {
            products.add(selectedProduct)
        }
    }

    fun addManyProducts(selectedProducts: MutableList<Product>) {
        //Could do this multiple ways. This way allows for utilizing the code we already have.
        for(product in selectedProducts) {
            addProduct(product)
        }
    }

    private fun removeProduct(product: Product) {
        products.remove(product)
    }

    fun removeProductById(productId: UUID) {
        for(product in products) {
            if(product.id == productId) {
                removeProduct(product)
                break
            }
        }
    }

    fun removeAllProducts() {
        products.clear()
    }

    fun addCoupon(coupon: Coupon) {
        coupons.add(coupon)
    }

    //We want to make sure that we apply coupons in a natural order before we apply them later.
    fun organizeCoupons() {
        val organizedCouponList = mutableListOf<Coupon>()

        for(coupon in coupons) {
            if(coupon.type == CouponType.FREE_PRODUCT) {
                organizedCouponList.add(coupon)
            }
        }

        for(coupon in coupons) {
            if(coupon.type == CouponType.BOGO_FREE) {
                organizedCouponList.add(coupon)
            }
        }

        for(coupon in coupons) {
            if(coupon.type == CouponType.FLAT_PRODUCT) {
                organizedCouponList.add(coupon)
            }
        }

        for(coupon in coupons) {
            if(coupon.type == CouponType.BOGO_FLAT) {
                organizedCouponList.add(coupon)
            }
        }

        for(coupon in coupons) {
            if(coupon.type == CouponType.PERCENTAGE_PRODUCT) {
                organizedCouponList.add(coupon)
            }
        }

        for(coupon in coupons) {
            if(coupon.type == CouponType.BOGO_PERCENTAGE) {
                organizedCouponList.add(coupon)
            }
        }

        for(coupon in coupons) {
            if(coupon.type == CouponType.FLAT_TOTAL) {
                organizedCouponList.add(coupon)
            }
        }

        for(coupon in coupons) {
            if(coupon.type == CouponType.PERCENTAGE_TOTAL) {
                organizedCouponList.add(coupon)
            }
        }

        coupons = organizedCouponList
    }

    fun applyCoupons() {
        for(coupon in coupons) {
            products = coupon.applyCoupon(products)
        }
    }

    fun getProducts(): MutableList<Product> {
        return products
    }

    fun getCoupons(): MutableList<Coupon> {
        return coupons
    }

    fun getTotal(): Float {
        return total
    }

    fun calculateTotal() {
        total = 0.00F

        for(product in products) {
            total += product.price
        }
    }
}