package order

import order.enums.CouponType
import product.Product
import java.util.*

class Coupon(val id: UUID, val type: CouponType, private val productId: UUID? = null) {
    private var percentageValue: Float? = null
    private var flatValue: Float? = null
    private var bogoBuyCount: Int? = null
    private var freeCount: Int? = null

    fun applyCoupon(productList: MutableList<Product>): MutableList<Product> {
        when(type) {
            CouponType.FLAT_PRODUCT -> {
                for(product in productList) {
                    if(product.id == productId && product.price > 0.00F) {
                        product.price -= flatValue!!
                        if (product.price < 0.00F) {
                            product.price = 0.00F
                        }
                        break
                    }
                }
            }
            CouponType.PERCENTAGE_PRODUCT -> {
                for(product in productList) {
                    if(product.id == productId && product.price > 0.00F) {
                        product.price *= percentageValue!!
                        break
                    }
                }
            }
            CouponType.BOGO_FREE -> {
                var productCount = 0

                for(product in productList) {
                    if(product.id == productId) {
                        productCount++
                        if(productCount % bogoBuyCount!! > 0) {
                            product.price = 0.00F
                        }
                    }
                }
            }
            CouponType.BOGO_FLAT -> {
                throw NotImplementedError(CouponType.BOGO_FLAT.name)
            }
            CouponType.BOGO_PERCENTAGE -> {
                throw NotImplementedError(CouponType.BOGO_PERCENTAGE.name)
            }
            CouponType.FREE_PRODUCT -> {
                var productCount = 0

                for(product in productList) {
                    if(product.id == productId) {
                        productCount++
                        if(productCount != freeCount) {
                            product.price = 0.00F
                        }
                    }
                }
            }
            CouponType.FLAT_TOTAL -> {
                for(product in productList) {
                    product.price -= flatValue?.div(productList.size)!!
                }
            }
            CouponType.PERCENTAGE_TOTAL -> {
                for(product in productList) {
                    product.price *= percentageValue!!
                }
            }
        }

        return productList
    }

    fun setFlatValue(couponValue: Float) {
        flatValue = couponValue
    }

    fun setPercentageValue(couponValue: Float) {
        percentageValue = couponValue
    }

    fun setBogoBuyCount(count: Int) {
        bogoBuyCount = count
    }

    fun setFreeCount(count: Int) {
        freeCount = count
    }

    fun getFlatValue(): Float {
        return flatValue!!
    }

    fun getPercentageValue(): Float {
        return percentageValue!!
    }

    fun getBogoBuyCount(): Int {
        return bogoBuyCount!!
    }

    fun getFreeCount(): Int {
        return freeCount!!
    }
}