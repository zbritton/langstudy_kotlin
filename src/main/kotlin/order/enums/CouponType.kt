package order.enums

enum class CouponType {
    PERCENTAGE_PRODUCT,
    PERCENTAGE_TOTAL,
    FREE_PRODUCT,
    FLAT_TOTAL,
    FLAT_PRODUCT,
    BOGO_FREE,
    BOGO_FLAT,
    BOGO_PERCENTAGE
}